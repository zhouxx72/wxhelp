let com = require('./utils/util.js')
App({
  com: com,
  onLaunch: function () {
    let _this = this
  },
  login(cb){
    wx.login({
      success(res) {
        com.post('wx/user/login', { js_code: res.code }, function (res) {
          console.log("用户信息",res)
          if (res.code == 200) {
            wx.setStorageSync("user", res.user)
            wx.setStorageSync("token", res.token)
            cb(res)
          }
        })
      }
    })
  },
  //获取默认地址
  getMoren(id){
    if(id){
      com.post('user/address/get/id', { id: id }, function (res) {
        if (res.code == 1) {
          wx.setStorageSync("address", res.data)
        }
      })
    }
  },
  getRes(id){
    com.get('wx/user/get/info/wxid', { wxId: id }, function (res) {
      if (res.code == 1) {
        wx.setStorageSync("res", res.data)
      }
    })
  },
  globalData: {
    userInfo: null
  }
})