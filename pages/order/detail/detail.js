const app = getApp()
let _this;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    wxId:wx.getStorageSync("user").id
  },
  comfirm(e) {
    let id = e.currentTarget.dataset.id
    wx.showLoading({
      title: '请稍等',
      task:true
    })
    app.com.post('help/confirm', { id: id }, function (res) {
      wx.hideLoading()
      if (res.code == 1) {
        wx.showToast({
          title: '订单已完成',
        })
        _this.getList(_this.data.list.id)
      } else {
        wx.showToast({
          title: '确认失败',
          icon: 'none'
        })
      }
    })
  },
  takeIt(e) {
    let msg = this.data.list
    console.log("takeIt",JSON.stringify(msg))
    if (wx.getStorageSync("res").state == 1) {
      this.takeDo(msg)
    } else {
      wx.showModal({
        title: '提示',
        content: '您还不是接单员，是否前往申请',
        confirmText: '立即前往',
        success(res) {
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/register/register',
            })
          }
        }
      })
    }


  },
  takeDo(msg) {
    wx.showLoading({
      title: '请稍等',
      task: true
    })
    
    app.com.post('help/jd', {
      jdId: wx.getStorageSync("user").id,
      id: msg.id,
      openid: msg.openid,
      formId: msg.formId,
      title: msg.title,
      orderNum: msg.orderNum
    }, function (res) {
      wx.hideLoading()
      if (res.code == 200) {
        wx.showToast({
          title: '接单成功',
        })
        _this.getList(_this.data.list.id)
      } else {
        wx.showToast({
          title: '接单失败',
          icon: 'none'
        })
      }
    })
  },
  cancel(e) {
    wx.showModal({
      title: '提示',
      content: '确定要取消吗？',
      success(res) {
        if (res.confirm) {
          wx.showLoading({
            title: '请稍等',
            task: true
          })
          app.com.cancel(e.currentTarget.dataset.id, 'navigateTo', function (res) {
            wx.hideLoading()
            if (res) {
              _this.getList(e.currentTarget.dataset.id)
            }
          })
        }
      }
    })

  },
  pay(e){
    app.com.post('help/pay',{
      title: e.currentTarget.dataset.title,
      openid:wx.getStorageSync("user").openid,
      oid: e.currentTarget.dataset.id,
      totalFee: e.currentTarget.dataset.price
    },function(res){
      if(res.code == 200){
        app.com.wxpay(res,function(res){
          wx.hideLoading()
          if (res) {
            _this.getList(_this.data.id)
          }
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    _this = this
    this.setData({
      id:options.id
    })
    this.getList(options.id)
  },
  getList(id) {
    app.com.get('help/get2', {
      fields: 'helplist.*,wxuser.phone,wxuser.dphone,wxuser.avatar_url,wxuser.nick_name',
      wheres:'helplist.id='+id,
      isAsc:'asc',
      orderByColumn:'helplist.create_time'
    }, function (res) {
      console.log("详情",JSON.stringify(res))
      if (res.code == 200) {
        let re = res.rows
        _this.setData({
          list: re[0],
          total: res.total
        })
        if(re[0].jdId){
          _this.getJd(re[0].jdId)
        }
      } else {
        wx.showToast({
          title: res.msg,
          icon: 'none'
        })
      }
    })
  },
  getJd(id){
    app.com.post('wx/user/get/id',{
      id: id
    },function(res){
      if(res.code == 200){
        _this.setData({
          jduser:res.data
        })
      }
    })
  },
  makePhoneCall(e){
    let ty = e.currentTarget.dataset.type
    if (this.data.wxId == this.data.list.wxId || this.data.wxId == this.data.list.jdId){
      wx.makePhoneCall({
        phoneNumber: this.data.list[ty],
      })
    }else{
      wx.showToast({
        title: '您不是该单的发布者或接单人',
        icon:'none'
      })
    }
    
  },
  makePhoneCall2(e) {
    let ty = e.currentTarget.dataset.type
    if (this.data.wxId == this.data.list.wxId || this.data.wxId == this.data.list.jdId) {
      wx.makePhoneCall({
        phoneNumber: this.data.jduser[ty],
      })
    } else {
      wx.showToast({
        title: '您不是该单的发布者或接单人',
        icon: 'none'
      })
    }
   
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  
})