const app = getApp()
const API = require('../../config/api.js')
let _this;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    load:false,
    search:'',
    wheres:''
  },
  searchInput(e){
    let search = e.detail.value
    if(search == ''){
      this.data.wheres = ''
    }else{
      this.data.wheres = 'is_delete=0 and name like "%'+search+'%"'
    }
    this.getArea()
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarColor({
      frontColor: '#ffffff',
      backgroundColor: '#6e42d3',
    })
    _this = this
    this.getArea()
  },
  checkedIt(e){
    let index = e.currentTarget.dataset.index;
    wx.showModal({
      title: '请确认您的选择',
      content: '您的选择是"' + _this.data.list[index].name+'"',
      cancelText:'我点错了',
      confirmText:'确认',
      confirmColor:'#6887e1',
      success(res){
        if(res.confirm){
          wx.setStorageSync('area', _this.data.list[index])
          _this.getDail(_this.data.list[index].pkId)
        }else{

        }
      }
    })
    
  },
  getArea(){
    this.setData({
      load:true
    })
    app.com.get(API.AreaUrl,{
      pageNum:1,
      pageSize:1000,
      wheres:this.data.wheres,
      isAsc:'asc',
      orderByColumn:'sort'
    },function(res){
      console.log("选择学校地址",res)
      if(res.code == 200){
        _this.setData({
          list: res.rows,
          load:false
        })
        
      }else{
        wx.showToast({
          title: '请求失败',
          icon:'none'
        })
      }
      
    })
  },
  getDail(aid){
    app.com.get(API.AidUrl,{
      aid:aid
    },function(res){
      console.log("开通校园",res)
      if(res.code == 200){
        wx.setStorageSync("dl", res.data)
        // _this.getServer(res.data.pk_id)
        wx.navigateBack({
          delta: 1
        })
      }else{
        wx.showToast({
          title: res.msg,
          icon:'none'
        })
      }
    })
  },  
  getServer(id){
    app.com.post(API.ServerUrl, {
      uid: id
    }, function (res) {
      if (res.code == 1) {
        wx.setStorageSync("server", res.data)
        wx.navigateBack({
          delta: 1
        })
      } else {
        wx.showToast({
          title: res.msg,
          icon: 'none'
        })
      }
    })
  },

})